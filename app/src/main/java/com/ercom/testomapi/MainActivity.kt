package com.ercom.testomapi

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.se.omapi.SEService
import android.util.Log
import java.util.concurrent.Executor

// Source : https://android.googlesource.com/platform/packages/apps/Camera2/+/master/src/com/android/camera/async/HandlerExecutor.java
class HandlerExecutor(private val mHandler: Handler) : Executor {
    override fun execute(runnable: Runnable) {
        mHandler.post(runnable)
    }
}

class TestOmapi(val context: Context) {
    val seService = SEService (context, HandlerExecutor(Handler(Looper.getMainLooper())), SEService.OnConnectedListener
    {
        connected()
    })

    fun connected() {
        Log.e("OMAPI", "Omapi connected")
        // The bug is here: 0 reader reported, and SIM / eSE readers are not availables.
        Log.e("OMAPI", "Number of readers: " + seService.readers.size)
    }
}

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val testOmapi = TestOmapi(this)
    }
}
